import React, { Component } from 'react';
import Uppy from '@uppy/core'
import Tus from '@uppy/tus'
import Webcam from '@uppy/webcam'
import DashboardModal from '@uppy/react/lib/DashboardModal'
import Dashboard from '@uppy/react/lib/Dashboard'
import '@uppy/core/dist/style.css'
import '@uppy/dashboard/dist/style.css'

export default class DragAndDropFileUploader extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      open: false
    }
    this.handleModalClick = this.handleModalClick.bind(this);

    this.uppy = Uppy({ id: 'uppy1', autoProceed: true, debug: true })
    .use(Webcam)
    .use(Tus)
      //.use(GoogleDrive, { companionUrl: 'https://companion.uppy.io' })
  }

  componentWillUnmount () {
    this.uppy.close();
  }

  handleModalClick () {
    this.setState({
      open: !this.state.open
    });
  }

  render () {
    const { open } = this.state;

    return (
        <div>
        <h1>Uppy Examples</h1>

       <h2>Dashboard Modale</h2>
        <div>
          <button id='dashboardmodal' onClick={this.handleModalClick}>
            {open ? 'Close dashboard' : 'Open dashboard'}
          </button>
          <DashboardModal
            uppy={this.uppy}
            open={open}
            target={document.body}
            onRequestClose={() => this.setState({ open: false })}
            //plugins={['Webcam']}
            trigger='#dashboardmodal'
          />
        </div>
       <h2>Dashboard Fissa</h2>
        <div>

          <Dashboard
            uppy={this.uppy}

            target={document.body}

            //plugins={['Webcam']}
            trigger='#dashboard'
          />
        </div>
      </div>
    )
  }
}